/* Dooba SDK
 * Analog Read
 */

#ifndef	__ARD_SCLI_H
#define	__ARD_SCLI_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Analog Read
extern void ard_scli(char **args, uint16_t *args_len);

#endif
