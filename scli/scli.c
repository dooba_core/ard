/* Dooba SDK
 * Analog Read
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "ard.h"
#include "scli/scli.h"

// Analog Read
void ard_scli(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	uint8_t p;

	// Acquire Pin Number
	if(str_next_arg(args, args_len, &x, &l) == 0)					{ scli_printf("Please provide a valid pin number\n"); return; }
	p = atoi(x);

	// Read
	scli_printf("Reading analog value on pin [%i]... ", p);
	p = ard(p);
	scli_printf("%i (0x%h)\n", p, p);
}
