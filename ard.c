/* Dooba SDK
 * Analog Read
 */

// Internal Includes
#include "ard.h"

// Init Analog Read
void ard_init()
{
	// Set Prescaler to 64 - gives ~150KHz @ 10MHz
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1);

	// Set AVCC Reference
	ADMUX |= (1 << REFS0);

	// Adjust to 8bit
	ADMUX |= (1 << ADLAR);

	// Start ADC
	ADCSRA |= (1 << ADEN);
	ADCSRA |= (1 << ADSC);
}

// Read analog value from pin
uint8_t ard(uint8_t pin)
{
	// Select ADC channel from pin
	switch(pin)
	{
		// Pins
		case ARD_PIN_0: pin = ARD_ADC_0; break;
		case ARD_PIN_1: pin = ARD_ADC_1; break;
		case ARD_PIN_2: pin = ARD_ADC_2; break;
		case ARD_PIN_3: pin = ARD_ADC_3; break;
		case ARD_PIN_4: pin = ARD_ADC_4; break;
		case ARD_PIN_5: pin = ARD_ADC_5; break;
		case ARD_PIN_6: pin = ARD_ADC_6; break;
		case ARD_PIN_7: pin = ARD_ADC_7; break;

		// Default
		default: return 0; break;
	}

	// Check Channel
	if(pin >= ARD_CHANS)							{ return 0; }

	// Clear & Set Mux
	ADMUX &= ~(ARD_CHAN_MASK);
	ADMUX |= pin;

	// Start Conversion
	ADCSRA |= (1 << ADSC);
	while(ADCSRA & (1 << ADSC));

	// Read
	return ADCH;
}
