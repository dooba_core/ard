/* Dooba SDK
 * Analog Read
 */

#ifndef	__ARD_H
#define	__ARD_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

// ADC-enabled Pins
#define	ARD_PIN_0				0
#define	ARD_PIN_1				1
#define	ARD_PIN_2				2
#define	ARD_PIN_3				3
#define	ARD_PIN_4				4
#define	ARD_PIN_5				5
#define	ARD_PIN_6				6
#define	ARD_PIN_7				7

// ADC Mapping
#define	ARD_ADC_0				7
#define	ARD_ADC_1				6
#define	ARD_ADC_2				5
#define	ARD_ADC_3				4
#define	ARD_ADC_4				3
#define	ARD_ADC_5				2
#define	ARD_ADC_6				1
#define	ARD_ADC_7				0

// ADC Channels
#define	ARD_CHANS				8
#define	ARD_CHAN_MASK			0x1f

/**
 * \brief Initialize Analog Input System
 */
extern void ard_init();

/**
 * \brief Read analog value from pin
 * \param pin which pin to read from (see the 'dio' library for pin mappings)
 * \return 8-bit analog voltage on pin
 */
extern uint8_t ard(uint8_t pin);

#endif
